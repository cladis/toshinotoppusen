﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using DotNetWikiBot;
using Newtonsoft.Json.Linq;

namespace ToshiNoToppuSen
{
    class Program
    {
        private static object threadLockConsole = new object();
        private static object threadLockStack = new object();
        private static object threadLockProgress = new object();
        private static object threadLockStatistics = new object();
        public static int progress = 0;
        public static int maxThreads = 200;
        public static Stack<Page> pagesStack = new Stack<Page>();
        private static AutoResetEvent waitHandle = new AutoResetEvent(false);
        public static Dictionary<Page, long> statisticsDict = new Dictionary<Page, long>();

        static void Main(string[] args)
        {
            int year = int.Parse(args[3]);
            Site wiki = new Site(String.Format("https://{0}.wikipedia.org", args[2]), args[0], args[1]);
            PageList pl = new PageList(wiki);
            pl.FillFromAllPages(null, 0, false, int.MaxValue);
            foreach (Page page in pl)
            {
                pagesStack.Push(page);
            }
            //Thread th = Thread.CurrentThread;


            for (int i = 0; i < maxThreads; i++)
            {
                Thread th = new Thread(new ParameterizedThreadStart(FetchYearStatistics))
                {
                    Name = "Thread #" + i
                };
                th.Start(new YearAndArticle
                {
                    Year = year,
                    Site = wiki
                });
            }

            waitHandle.WaitOne();
            wiki = new Site(String.Format("https://{0}.wikipedia.org", args[2]), args[0], args[1]);
            SaveTheStats(year, wiki);
            Console.ReadLine();
        }

        /// <summary>
        /// Oders the global Dictionary by number of views and saves
        /// the top 1000 items to the wiki specified
        /// </summary>
        /// <param name="year">year in question. Used to build correct output pagename</param>
        /// <param name="wiki">wiki to write to</param>
        private static void SaveTheStats(int year, Site wiki)
        {
            var top = from entry in statisticsDict orderby entry.Value descending select entry;
            StringBuilder wout = new StringBuilder();
            wout.AppendLine(
@"Найвідвідуваніші статті Вікіпедії за рік
* усі пристрої
* усі типи юзерагентів

Інформація з [https://wikimedia.org/api/rest_v1/#/ Wikimedia Rest API].

{| class='wikitable sortable'
|-
! № !! Стаття !! Переглядів за рік
"
            );

            int counter = 0;
            foreach (var item in top)
            {
                string title = item.Key.title;
                long views = item.Value;
                counter++;
                wout.AppendFormat("|-\n| {2} || [[{0}]] || {1}\n", title, views, counter);

                if (counter == 1005)
                {
                    break;
                }
            }
            wout.AppendLine("|}");

            Page output = new Page(wiki, String.Format("user:BaseBot/Статистика переглядів/Топ1000/{0}", year))
            {
                text = wout.ToString()
            };
            output.Save();
        }

        /// <summary>
        /// Overload for proceeding single object typed parameter
        /// to be used with ParameterizedThreadStart
        /// </summary>
        /// <param name="yearAndArticle"></param>
        static void FetchYearStatistics(object yearAndArticle)
        {
            bool goon = false;
            if (yearAndArticle is YearAndArticle)
            {
                Page article = null;
                lock (threadLockStack)
                {
                    int count = pagesStack.Count;
                    goon = count > 1;
                    if (count > 0)
                    {
                        article = pagesStack.Pop();
                    }
                    goon = count > 1;
                }
                if (article != null)
                {
                    try
                    {
                        FetchYearStatistics(
                                ((YearAndArticle)yearAndArticle).Year,
                                article,
                                ((YearAndArticle)yearAndArticle).Site
                        );
                    }
                    catch (Exception ex)
                    {
                        lock (threadLockConsole)
                        {
                            CurrentThreadPrefix();
                            ConsoleColor oldColor = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Something was wrong with {0}. {1}", article.title, ex.Message);
                            Console.ForegroundColor = oldColor;
                        }
                    }
                }
            }
            if (goon)
            {
                FetchYearStatistics(yearAndArticle);
            }
            lock (threadLockProgress)
            {
                progress++;
                if (progress == maxThreads)
                {
                    waitHandle.Set();
                }
            }
        }

        /// <summary>
        ///  Overloading since I know I will be lazy.
        /// </summary>
        /// <param name="article"></param>
        static void FetchYearStatistics(Page article, Site wiki)
        {
            FetchYearStatistics(DateTime.Now.AddYears(-1).Year, article, wiki);
        }

        /// <summary>
        /// Fetches view statistics for a given article in a given year and wiki
        /// from Wikimedia REST API. Proceeds the JSON obtained and adds the result
        /// to the global Dictionary
        /// </summary>
        /// <param name="year">the year in question</param>
        /// <param name="article">article to get the stats for</param>
        /// <param name="wiki">Wikimedia wiki it is all about</param>
        static void FetchYearStatistics(int year, Page article, Site wiki)
        {
            //lock (threadLockConsole)
            //{
            //    CurrentThreadPrefix();
            //    Console.WriteLine(String.Format("Fetching {0} statistics for {1}", year, article.title));
            //}
            Uri wikiUri = new Uri(wiki.address);
            string encodedTitle = HttpUtility.UrlEncode(article.title.Replace(" ", "_"));
            string url = String.Format(@"https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/{0}/all-access/all-agents/{1}/daily/{2}0101/{2}1231", wikiUri.Host, encodedTitle, year);
            //Console.WriteLine(url);
            string statRaw = wiki.GetWebPage(url);
            JObject stat = JObject.Parse(statRaw);
            long sum = (from i in stat["items"].Children()["views"] select i.Value<long>()).Sum();
            //lock (threadLockConsole)
            //{
            //    CurrentThreadPrefix();
            //    Console.WriteLine(String.Format("Article '{0}'; Views: {1}", article.title, sum));
            //}

            lock (threadLockStatistics)
            {
                statisticsDict.Add(article, sum);
            }
        }

        /// <summary>
        /// Small useless method to write current thread name
        /// in a fancy manner as a prefix for following output
        /// </summary>
        public static void CurrentThreadPrefix()
        {
            ConsoleColor old = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.Write("{0}: ", Thread.CurrentThread.Name);
            Console.ForegroundColor = old;
        }
    }

    /// <summary>
    /// A struct to be used for passing args as one object variable
    /// </summary>
    struct YearAndArticle
    {
        public int Year { get; set; }
        public Site Site { get; set; }
    }
}